<?php
/*
Plugin Name: Opti World Currency TinyMCE
Plugin URI: https://www.eastcapetours.com/ect/opti-world-currency-tinymce/
Version: 1.0
Author: Simon Urban
Author URI: https://www.eastcapetours.com/ect/simon-urban/
Description: A simple TinyMCE Plugin to add an Opti World Currency shortcode in the Visual Editor.
License: GPLv2

Opti World Currency TinyMCE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Opti World Currency TinyMCE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Opti World Currency TinyMCE. If not, see https://www.gnu.org/licenses/gpl-2.0.html
*/

class OWC_TinyMCE_Class {
	function __construct() {
    if (is_admin()) {
      add_action('init', array($this, 'setup_tinymce_plugin'));
    }
  }

  // Check if the current user can edit Posts or Pages and is using the Visual Editor
  function setup_tinymce_plugin() {
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
      return;
    }
    if (get_user_option('rich_editing') !== 'true') {
      return;
    }

    add_filter('mce_external_plugins', array(&$this, 'add_tinymce_plugin'));
    add_filter('mce_buttons', array(&$this, 'add_tinymce_toolbar_button'));
  }

  // Loads the TinyMCE compatible JS file
  function add_tinymce_plugin($plugin_array) {
    $plugin_array['worldcurrency'] = plugin_dir_url(__FILE__) . 'opti-world-currency-tinymce.js';
    return $plugin_array;
  }

  // Add button to TinyMCE toolbar
  function add_tinymce_toolbar_button($buttons) {
    array_push($buttons, '|', 'worldcurrency');
    return $buttons;
  }
}

$owc_tinymce_class = new OWC_TinyMCE_Class;
