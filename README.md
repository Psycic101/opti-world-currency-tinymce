# Opti World Currency TinyMCE #

### About ###

* Version 1.0
* Opti World Currency TinyMCE is a WordPress plugin that provides a TinyMCE button and interface for generating shortcodes for Opti World Currency. It should also work with World Currency.

### How do I get set up? ###

* Download the repo as a zip file.
* Upload the zipped plugin to your WordPress install.
* Activate Opti World Currency TinyMCE.
* Enjoy.

### Credits ###

* Created by Simon Urban

### Changelog ###

.: 1.0 :.

* Initial codebase upload.
